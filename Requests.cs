﻿using System;
using System.IO;
using System.Net;
using System.Web;

namespace Connection 
{
    public class Requests
    {

      
        public string GET(string url) => GetResponse(url);
        /// <summary>
        ///<para>Tipo determina qual sera a chamada realizada.</para>
        /// <para>Onde as opções são:</para>
        ///        <para> Token: Emite um token de acesso para comunicação de todos os endpoints, necessário em todas as chamadas da API</para>
        ///        <para> Acknowledgment: Envia uma requisição confirmando o recebimento dos eventos.</para>
        ///        <para> integration: Informa ao IFood que o pedido foi integrado pelo e-PDV.
        ///              Integração significa que o e-PDV foi capaz de realizar o parse do pedido e integrar em seu sistema..</para>
        ///        <para> Confirmation: Informa ao IFood que o pedido foi confirmado pelo e-PDV.</para>     
        ///        <para> Dispatch: Informa ao IFood que o pedido saiu para ser entregue ao cliente.</para>     
        ///        <para> Delivery: Informa ao IFood que o pedido foi entregue ao cliente.</para>
        ///        <para> Rejection: Informa ao IFood que o pedido foi rejeitado pelo e-PDV.</para>
        ///        
        ///        
        /// </summary>
        /// <returns>String formato json.</returns>
        /// 
        public string Post(string tipo) => Operacao(tipo);
       

        private static string Operacao(string tipo)
        {
            switch (tipo.ToUpper())
            {
                case "TOKEN":
                    return GetKeyCode();
                case "ACKNOWLEDGMENT":
                    return GetKeyCode();
                case "INTEGRATION":
                    return GetKeyCode();
                case "CONFIRMATION":
                    return GetKeyCode();
                case "DISPATCH":
                    return GetKeyCode();
                case "DELIVERY":
                    return GetKeyCode();
                case "REJECTION":
                    return GetKeyCode();
                default:
                    return "Opção invalida";
            }
        }

        private static string GetResponse(string url)
        {
            try
            {
               var request = WebRequest.Create(url);
                WebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    return reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        private static string GetKeyCode()
        {
            try{ 
                

                string t1 = File.ReadAllText("../../cliente.json");
                Cliente cliente = Cliente.FromJson(t1);
                string t2 = File.ReadAllText("../../token.json");
                //Token token = Token.FromJson(t2);
                //return token.AccessToken;

                string url = "https://pos-api.ifood.com.br/oauth/token?client_id=" + cliente.ClientId + "&client_secret="+ HttpUtility.UrlEncode(cliente.ClientSecret)+"&username="+cliente.Username+ "&password=" + cliente.Password+"&grant_type=" + cliente.GrantType;
                var request = WebRequest.Create(url);
                request.ContentType = "application/json; charset=utf-8";
                request.Method = "POST";
                WebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                    string Return = reader.ReadToEnd();
                    string path =("../../token.json");
                    File.WriteAllText(path, Return);
                    
                    return "a";

                }
            }
            catch (WebException ex){
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    return reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
          
        }
    }
}
